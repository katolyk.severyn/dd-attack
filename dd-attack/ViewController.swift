//
//  ViewController.swift
//  dd-attack
//
//  Created by Severyn-Wsevolod on 28.02.2022.
//

import UIKit

final class ViewController: UIViewController {
    
    @IBOutlet private weak var logsTextView: UITextView!
    @IBOutlet private weak var versionLabel: UILabel!
    private lazy var checker = Checker(delegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.isIdleTimerDisabled = true
        versionLabel.text = "Version " + Bundle.main.version
        logsTextView.layoutManager.allowsNonContiguousLayout = false
        checker.start()
    }
    
    deinit {
        UIApplication.shared.isIdleTimerDisabled = false
    }
}

// MARK: - CheckerDelegate
extension ViewController: CheckerDelegate {
    func log(text: String) {
        DispatchQueue.main.async {
            let newLogText = (self.logsTextView.text ?? "\n") + text + "\n"
            
            print(text)
            self.logsTextView.text = newLogText
            
            if self.logsTextView.isScrollInBottom {
                self.logsTextView.scrollRangeToVisible(NSMakeRange(newLogText.count - 1, 0))
            }
        }
    }
}
