//
//  Networking.swift
//  dd-attack
//
//  Created by Severyn-Wsevolod on 02.03.2022.
//

import Foundation

struct Networking {
    private init() {  }
    
    static func fetchHosts(completion: @escaping (Result<[String], DDError>) -> Void) {
        loadJson(fromURL: "https://gitlab.com/cto.endel/atack_hosts/-/raw/master/hosts.json") { result in
            switch result {
            case .success(let data):
                guard let hosts = try? JSONDecoder().decode([String].self, from: data) else {
                    return completion(.failure(.generalError(message: "The server response was not recognized.")))
                }
                
                completion(.success(hosts))
                
            case .failure(let error):
                completion(.failure(.generalError(message: error.localizedDescription)))
            }
        }
    }
    
    static func fetchHostInfo(url hostUrl: String, completion: @escaping (Result<HostInfo, DDError>) -> Void) {
        loadJson(fromURL: hostUrl) { result in
            switch result {
            case .success(let data):
                print(String(decoding: data, as: UTF8.self))
                guard let hostInfo = try? JSONDecoder().decode(HostInfo.self, from: data) else {
                    return completion(.failure(.generalError(message: "The server response was not recognized.")))
                }
                
                completion(.success(hostInfo))

            case .failure(let error):
                completion(.failure(.generalError(message: error.localizedDescription)))
            }
        }
    }
    
    static func loadJson(fromURL urlString: String, completion: @escaping (Result<Data, DDError>) -> Void) {
        URLSession(configuration: .default).dataTask(with: URL(string: urlString)!) { data, _, error in
            if let error = error {
                return completion(.failure(.generalError(message: error.localizedDescription)))
            }
            
            data.flatMap { completion(.success($0)) }
        }.resume()
    }
}
