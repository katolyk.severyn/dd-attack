//
//  DDError.swift
//  dd-attack
//
//  Created by Severyn-Wsevolod on 02.03.2022.
//

import Foundation

enum DDError: Error, CustomStringConvertible {
    enum Code {
        static let timeOut = -1001
        static let ok = 200
        static let proxyAuthenticationRequired = 407
    }
    
    case generalError(message: String)
    
    var description: String {
        switch self {
        case .generalError(let message): return message
        }
    }
}
