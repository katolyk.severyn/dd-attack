//
//  HostInfo.swift
//  dd-attack
//
//  Created by Severyn-Wsevolod on 02.03.2022.
//

import Foundation

struct HostInfo: Decodable {
    let site: SiteInfo
    let proxies: [ProxyInfo]
    
    private enum CodingKeys: String, CodingKey {
        case site
        case proxies = "proxy"
    }
}

struct SiteInfo: Decodable {
    let id: Int
    let url: String
    let needParseUrl: Int?
    let page: String
    let pageTime: Int?
    let atack: Int?
    
    public init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try map.decode(Int.self, forKey: .id)
        self.url = try map.decode(String.self, forKey: .url)
        self.needParseUrl = try map.decode(Int.self, forKey: .needParseUrl)
        self.page = try map.decode(String.self, forKey: .page)
        self.pageTime = try map.decode(Int.self, forKey: .pageTime)
        self.atack = try map.decode(Int.self, forKey: .atack)
    }
    
    private enum CodingKeys: String, CodingKey {
        case id
        case url
        case needParseUrl = "need_parse_url"
        case page
        case pageTime = "page_time"
        case atack
    }
}

struct ProxyInfo: Decodable {
    let id: Int
    let auth: String
    
    let host: String
    let port: Int
    
    let login: String
    let password: String
    
    public init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        let anotherContainer = try decoder.container(keyedBy: AdditionalCodingKeys.self)
        
        self.id = try map.decode(Int.self, forKey: .id)
        self.auth = try map.decode(String.self, forKey: .auth)
        
        let authComponents = auth.components(separatedBy: ":")
        self.login = authComponents[0]
        self.password = authComponents[1]
        
        let ip = try anotherContainer.decode(String.self, forKey: .ip)
        
        let proxyAddress = ip.components(separatedBy: ":")
        self.host = proxyAddress[0]
        self.port = Int(proxyAddress[1].replacingOccurrences(of: "\r", with: ""))!
    }
    
    private enum CodingKeys: String, CodingKey {
        case id
        case auth
    }
    
    private enum AdditionalCodingKeys: String, CodingKey {
        case ip
    }
}
