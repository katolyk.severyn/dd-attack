//
//  ViewController.swift
//  dd-attack
//
//  Created by Severyn-Wsevolod on 01.03.2022.
//

import Foundation

extension Bundle {
    var release: String {
        return (infoDictionary?["CFBundleShortVersionString"] as? String) ?? "x.x.x"
    }
    
    var build: String {
        return (infoDictionary?["CFBundleVersion"] as? String) ?? "xx"
    }
    
    var version: String {
        return "\(release)-\(build)"
    }
}
