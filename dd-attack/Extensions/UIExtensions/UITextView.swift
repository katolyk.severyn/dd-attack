//
//  UITextView.swift
//  dd-attack
//
//  Created by Severyn-Wsevolod on 02.03.2022.
//

import UIKit

extension UITextView {
    var isScrollInBottom: Bool {
        contentOffset.y + frame.size.height + 60 >= contentSize.height
    }
}
